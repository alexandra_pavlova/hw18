#ifndef STACK_H
#define STACK_H
#include <cassert> 
#include <iostream>
#include <iomanip> 

template <typename T>
class Stack
{
private:
    T* stackPtr;                      // ��������� �� ����
    const int size;                   // ������������ ���������� ��������� � �����
    int top;                          // ����� �������� �������� �����
public:
    Stack(int = 10);                  // ������ ����� �� ���������: 10
    Stack(const Stack<T>&);           // ����������� �����������
    ~Stack();                         // ����������

    inline void push(const T&);       // �������� �������
    inline T pop();                   // ������� ������� �������
    inline void printStack();         // ����� ����� �� �����

    inline int getStackSize() const;  // �������� ������ �����
    inline T* getPtr() const;         // �������� ��������� �� ����
    inline int getTop() const;        // �������� ����� �������� �������� � �����
};

// ���������� ������� ������� ������ Stack:

                                      // 1) ����������� �����
template <typename T>
Stack<T>::Stack(int maxSize) :
    size(maxSize) 
{
    stackPtr = new T[size]; 
    top = 0;
}
                                      // 2) ����������� �����������
template <typename T>
Stack<T>::Stack(const Stack<T>& otherStack) :
    size(otherStack.getStackSize()) 
{
    stackPtr = new T[size]; 
    top = otherStack.getTop();

    for (int ix = 0; ix < top; ix++)
        stackPtr[ix] = otherStack.getPtr()[ix];
}
                                      // 3) ����������
template <typename T>
Stack<T>::~Stack()
{
    delete[] stackPtr;
}
                                      // 4) �������� �������
template <typename T>
inline void Stack<T>::push(const T& value)
{
    assert(top < size); 
    stackPtr[top++] = value; 
}
                                      // 5) ������� ������� �������
template <typename T>
inline T Stack<T>::pop()
{
    assert(top > 0); 
    stackPtr[--top];
    return 0;
}
                                      // 6) ����� ����� �� �����
template <typename T>
inline void Stack<T>::printStack()
{
    for (int ix = top - 1; ix >= 0; ix--)
        std::cout << stackPtr[ix] << '\n';
}
                                      // 7) �������� ������ �����
template <typename T>
inline int Stack<T>::getStackSize() const
{
    return size;
}
                                      // 8) �������� ��������� �� ���� 
template <typename T>
inline T* Stack<T>::getPtr() const
{
    return stackPtr;
}
                                      //9) �������� ������ �����
template <typename T>
inline int Stack<T>::getTop() const
{
    return top;
}

#endif // STACK_H

#include <iostream>

using namespace std;



int main()
{
    cout << "Size of stack: ";
    int SizeOfStack;
    cin >> SizeOfStack;
    Stack<int> stackSymbol(SizeOfStack);
    int ct = 0;
    int ch;

    cout << "Stack elements: ";
    while (ct++ < SizeOfStack)
    {
        cin >> ch;
        stackSymbol.push(ch); 
    }
    cout << '\n' << "Our stack: " << '\n';
    stackSymbol.printStack(); 

    cout << '\n' << "Delete element from stack:" << '\n';
    stackSymbol.pop();
    stackSymbol.printStack(); 
    cout << '\n';

    cout << "Add new element to stack:" << '\n';
    stackSymbol.push(14);
    Stack<int> newStack(stackSymbol);
    newStack.printStack();

    return 0;
}